export class Eventos {
    public name: string;
    public date: string;
    public time: string;
    public location: Location;
}

export class Location {
    public adress: string;
    public city: string;
    public country: string;
}
