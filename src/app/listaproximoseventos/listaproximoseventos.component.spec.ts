import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaproximoseventosComponent } from './listaproximoseventos.component';

describe('ListaproximoseventosComponent', () => {
  let component: ListaproximoseventosComponent;
  let fixture: ComponentFixture<ListaproximoseventosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaproximoseventosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaproximoseventosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
