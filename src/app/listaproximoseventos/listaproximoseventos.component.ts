import { Component, OnInit } from '@angular/core';
import { Eventos, Location } from '../datos';

@Component({
  selector: 'app-listaproximoseventos',
  templateUrl: './listaproximoseventos.component.html',
  styleUrls: ['./listaproximoseventos.component.css']
})
export class ListaproximoseventosComponent implements OnInit {

  location: Location = { adress: 'Zeballos',
                         city: 'Rosario',
                         country: 'Argentina'};
  evento: Eventos = { name: 'Primer evento',
                      date: '27/10/2018',
                      time: '12hs',
                      location: this.location};



  constructor() {
  // this.location = { adress: 'zeballos', city: 'Rosario', country: 'Argentina'};
  // this.evento = { name: 'Primer evento', date: '27/10/2018', time: '12hs', location: location};
  }

  ngOnInit() {
  }

}
